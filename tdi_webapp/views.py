# importing required packages
from django.http import HttpResponse
from django.template import loader
from django.views.decorators.csrf import csrf_exempt
import os
from django.core.files.storage import FileSystemStorage

@csrf_exempt
def index(request):
    template = loader.get_template('index.html')
    return HttpResponse(template.render())


@csrf_exempt
def info(request):
    if request.method == 'POST':
        #file = save_file(request.FILES['uploadFromPC'])
        #request.session['file_name'] = file

        return thanks(request)

    template = loader.get_template('info.html')
    return HttpResponse(template.render())


@csrf_exempt
def resize(request):
    template = loader.get_template('resize.html')
    return HttpResponse(template.render())


@csrf_exempt
def thanks(request):
    if request.method == 'POST' and request.FILES['uploadFromPC']:
        #request.session['file_name'] = file
        file = request.FILES['uploadFromPC']
        fs = FileSystemStorage()
        filename = fs.save(file.name, file)
        uploaded_file_url = fs.url(filename)

        template = loader.get_template('thanks.html')
        html = template.render({'file_path': uploaded_file_url})
        return HttpResponse(html)


@csrf_exempt
def test_upload(request):
    if request.method == 'POST':
        save_file(request.FILES['uploadFromPC'])
        # file1 = request.FILES['uploadFromPC']
        #
        # filename = file1._get_name()
        # fd = open('%s/%s' % (MEDIA_ROOT, str(filename)), 'wb')
        # for chunk in file.chunks():
        #     fd.write(chunk)
        # fd.close()

        template = loader.get_template('test_upload.html')

        return HttpResponse(template.render())

    template = loader.get_template('test_upload.html')
    return HttpResponse(template.render())


@csrf_exempt
def test_upload1(request):
    if request.method == 'POST':
        save_file(request.FILES['uploadFromPC'])
        # file1 = request.FILES['uploadFromPC']
        #
        # filename = file1._get_name()
        # fd = open('%s/%s' % (MEDIA_ROOT, str(filename)), 'wb')
        # for chunk in file.chunks():
        #     fd.write(chunk)
        # fd.close()

        template = loader.get_template('test_upload1.html')

        return HttpResponse(template.render())

    template = loader.get_template('test_upload1.html')
    return HttpResponse(template.render())


def save_file(file1, path=''):
    ''' Little helper to save a file
    '''
    # filename = file1.name
    # fd = open('%s/%s' % (MEDIA_ROOT, str(path) + str(filename)), 'wb')
    # for chunk in file.chunks():
    #     fd.write(chunk)
    # fd.close()
    #
    # fd = open('%s/%s' % (MEDIA_ROOT, str(path) + str(filename)), 'wb')
    # file.save(fd)
    #
    fs = FileSystemStorage()
    fs.save(file1.name, file1)
    return file1.name
