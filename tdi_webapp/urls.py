"""tdi_webapp URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.conf import settings
from django.conf.urls.static import static
from . import views

urlpatterns = [
    path('admin/', admin.site.urls, name='admin'),
    path('test_upload/', views.test_upload, name='test_upload'),
    path('test_upload1/', views.test_upload1, name='test_upload1'),
    path('info/', views.info, name='info'),
    path('resize/', views.resize, name='resize'),
    path('thanks/', views.thanks, name='thanks'),
    # path('document/', views.document, name='document'),
    path('', views.index, name='index')
]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
